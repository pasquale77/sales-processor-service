package operation;

import model.Product;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This is the base class for each type of operation
 *
 */
abstract class AbstractOperation implements Operation {

    @Override
    public boolean isApplicable(String operation) {

        return false;
    }

    @Override
    public Double getTotalPriceAdjustment(String operation, Product product) {

        return null;
    }

    @Override
    public String priceAdjustment(String operation, double adjustedPrice, Product product){

        String adjustmentRow = String.format(
            "%s %.2fp to %d %s - price passed from %.2fp to %.2fp",
            operation, product.getProductPrice(),
            product.getTotalQuantity(), product.getProductType(),
            product.getTotalPrice(), adjustedPrice
        );

        return adjustmentRow;
    }

}
