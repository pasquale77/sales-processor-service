package operation;

import model.Product;

/**
 * Created by plongobucco on 24/02/18.
 */
public interface Operation {

    boolean isApplicable(String operation);

    Double getTotalPriceAdjustment(String operation, Product product);

    String priceAdjustment(String operation, double priceAdjusted, Product product);

}
