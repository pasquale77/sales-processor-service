package operation;

import model.Product;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This class describe the type of Multiply operation
 *
 */
public class MultiplyOperation extends AbstractOperation {

    private final String OPERATION = "Multiply";

    @Override
    public boolean isApplicable(String operation) {

        return null != operation && operation.equalsIgnoreCase(OPERATION);
    }

    @Override
    public Double getTotalPriceAdjustment(String operation, Product product) {

        return product.getTotalPrice() +
                (product.getTotalPrice() * product.getProductPrice()) +
                (product.getTotalQuantity() * product.getProductPrice());

    }
}
