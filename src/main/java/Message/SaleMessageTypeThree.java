package Message;

import model.Product;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This class describe the type of message three, e.g Add 20p apples
 *
 */
public class SaleMessageTypeThree extends AbstractSaleMessage {

    String[] operations = {"Add", "Subtract", "Multiply"};

    private Predicate<String> predicateThree = e -> Arrays.stream(operations).anyMatch(e::contains);

    @Override
    protected Product doParseSale(String sale, Product product) {

        String[] salesDetails = sale.split("\\s+");

        product.setProductPrice(parsePrice(salesDetails[1]));
        product.setTotalQuantity(0);

        return product;
    }

    @Override
    protected boolean checkSale(String sale){

        if (sale == null || sale.isEmpty()) {

            return false;
        }

        return true;
    }

    @Override
    public boolean isApplicable(String sale) {

        return Stream.of(sale).anyMatch(predicateThree);
    }

    @Override
    public String productType(String sale) {

        String[] salesDetails = sale.split("\\s+");
        return salesDetails[2];
    }

    @Override
    public String operationType(String sale) {

        String[] salesDetails = sale.split("\\s+");
        return salesDetails[0];

    }
}
