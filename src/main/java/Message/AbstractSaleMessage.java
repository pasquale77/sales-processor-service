package Message;

import model.Product;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This is the base class for each type of message
 *
 */
abstract class AbstractSaleMessage implements SaleMessage {

    public Product parseSale(String sale, Product product){

        checkSale(sale);
        return doParseSale(sale, product);

    }

    protected abstract Product doParseSale(String sale, Product product);
    protected abstract boolean checkSale(String sale);

    protected String parseType(String productType) {

        String resultProductType;
        String typeWithoutLastChar = productType.substring(0, productType.length() - 1);

        if (productType.endsWith("o")) {
            resultProductType = String.format("%soes", typeWithoutLastChar);
        } else if (productType.endsWith("h")) {
            resultProductType = String.format("%shes", typeWithoutLastChar);
        } else if (!productType.endsWith("s")) {
            resultProductType = String.format("%ss", productType);
        } else if (productType.endsWith("y")) {
            resultProductType = String.format("%sies", typeWithoutLastChar);
        } else {
            resultProductType = String.format("%s", productType);
        }
        return resultProductType.toLowerCase();

    }

    protected double parsePrice(String productPrice) {

        double price = Double.parseDouble(productPrice.replaceAll("p", ""));

        if (!productPrice.contains(".")) {
            price = price / Double.valueOf("100");
        }

        return price;
    }

}
