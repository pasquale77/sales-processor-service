package Message;

import model.Product;

/**
 * Created by plongobucco on 24/02/18.
 */
public interface SaleMessage {

    boolean isApplicable(String sale);

    String productType(String sale);

    String operationType(String sale);

    Product parseSale(String sale, Product product);

}
