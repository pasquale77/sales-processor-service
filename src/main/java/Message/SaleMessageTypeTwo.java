package Message;

import model.Product;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This class describe the type of message two, e.g 20 sales of apples at 10p each.
 *
 */
public class SaleMessageTypeTwo extends AbstractSaleMessage {

    private Predicate<String> predicateTwo = e -> e.matches("^\\d.*$");

    @Override
    protected Product doParseSale(String sale, Product product) {

        String[] salesDetails = sale.split("\\s+");

        product.setProductPrice(parsePrice(salesDetails[5]));
        product.setProductQuantity(Integer.parseInt(salesDetails[0]));
        product.setTotalQuantity(Integer.parseInt(salesDetails[0]));

        return product;
    }

    @Override
    protected boolean checkSale(String sale){

        if (sale == null || sale.isEmpty()) {

            return false;
        }

        return true;
    }

    @Override
    public boolean isApplicable(String sale) {

        return Stream.of(sale).allMatch(predicateTwo);
    }

    @Override
    public String productType(String sale) {

        String[] salesDetails = sale.split("\\s+");
        return parseType(salesDetails[3]);
    }

    @Override
    public String operationType(String sale) {
        return null;
    }
}
