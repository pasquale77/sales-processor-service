package Message;

import model.Product;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This class describe the type of message one, e.g apple at 10p
 *
 */
public class SaleMessageTypeOne extends AbstractSaleMessage {

    private Predicate<String> predicateOne = e -> Stream.of(e.split(" ")).count() == 3 && e.contains("at");

    @Override
    protected Product doParseSale(String sale, Product product) {

        String[] salesDetails = sale.split("\\s+");

        product.setProductType(parseType(salesDetails[0]));
        product.setProductPrice(parsePrice(salesDetails[2]));
        product.setProductQuantity(1);
        product.setTotalQuantity(1);

        return product;
    }

    @Override
    protected boolean checkSale(String sale){

        if (sale == null || sale.isEmpty()) {

            return false;
        }

        return true;
    }

    @Override
    public boolean isApplicable(String sale) {

        return Stream.of(sale).allMatch(predicateOne);
    }

    @Override
    public String productType(String sale) {

        String[] salesDetails = sale.split("\\s+");
        return parseType(salesDetails[0]);
    }

    @Override
    public String operationType(String sale) {
        return null;
    }
}
