import processor.SaleProcessor;

/**
 * Created by plongobucco on 23/02/18.
 *
 * Main class for processing messages contained in a text file
 *
 */
public class SalesProcessor {

    public static void main(String[] args) {

        SaleProcessor salesProcessor = new SaleProcessor();
        salesProcessor.init();
        salesProcessor.processSales("file/sales.txt");

    }

}
