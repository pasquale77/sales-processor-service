package processor;

import model.Product;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This is the base class to process messages
 *
 */
abstract class AbstractSalesProcessor {

   private String sale;
   protected HashMap<String, Product> salesByType = new HashMap<>();

    /**
     * This method process the sales
     *
     * @param fileName
     *
     */
   public final void processSales(String fileName){

       readSalesFromFile(fileName);
       parseSale(sale);

   }

   /**
     * parse one sale
     *
     * @param sale
     *
     */
   abstract void parseSale(String sale);

   /**
     * Log the sales
     *
     */
   abstract void logging();

    /**
     * Reads sales from file
     *
     * @param fileName
     *
     */
   protected void readSalesFromFile(String fileName){

       try (BufferedReader reader = new BufferedReader(new FileReader(ClassLoader.getSystemResource(fileName).getPath()))) {

           String sale;

           while ((sale = reader.readLine()) != null) {
               this.sale = sale;
               parseSale(this.sale);
               logging();
           }

       } catch (IOException e) {

           System.out.println(e.getMessage());

       }
       System.exit(1);
   }

    /**
     * This method return one product by type
     *
     * @param productType
     *
     */
   protected Product getProduct(String productType) {

        Product product = salesByType.getOrDefault(productType, new Product(productType));

        return product;
   }

    /**
     * This method update one specific product
     *
     * @param product
     *
     */
    protected void updateProduct(Product product){

        salesByType.put(product.getProductType(), product);
   }

}
