package processor;

import Message.SaleMessage;
import Message.SaleMessageTypeOne;
import Message.SaleMessageTypeThree;
import Message.SaleMessageTypeTwo;
import model.Product;
import operation.AddOperation;
import operation.MultiplyOperation;
import operation.Operation;
import operation.SubtractOperation;
import report.Report;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This class is used to process sales
 *
 */
public class SaleProcessor extends AbstractSalesProcessor {

    protected int processedMessages = 0;

    protected List<Operation> mOperation;
    protected List<SaleMessage> mSaleMessageType = new ArrayList<>();
    protected List<String> mAdjustedList = new ArrayList<>();

    /**
     * Initialization method for building message types and operations
     *
     */
    public void init(){

        mSaleMessageType.add(new SaleMessageTypeOne());
        mSaleMessageType.add(new SaleMessageTypeTwo());
        mSaleMessageType.add(new SaleMessageTypeThree());

        mOperation = new ArrayList<>();
        mOperation.add(new AddOperation());
        mOperation.add(new SubtractOperation());
        mOperation.add(new MultiplyOperation());
    }

    @Override
    void parseSale(String sale) {

        if(checkSale(sale)){

            Product product = null;
            String operationType = "";
            Double adjustPrice = null;

            for (SaleMessage typeMessage : mSaleMessageType) {
                if (typeMessage.isApplicable(sale)) {
                    product = typeMessage.parseSale(sale, getProduct(typeMessage.productType(sale)));
                    operationType = typeMessage.operationType(sale);
                }
            }

            if(null != operationType && !operationType.isEmpty()){
                for (Operation operation : mOperation) {
                    if (operation.isApplicable(operationType)) {
                        adjustPrice = operation.getTotalPriceAdjustment(operationType, product);
                        mAdjustedList.add(operation.priceAdjustment(operationType, adjustPrice, product));
                    }
                }
                operationType = "";
            }
            product = getTotalPrice(adjustPrice, product);
            updateProduct(product);
            processedMessages++;
        }
    }

    protected boolean checkSale(String sale){

        if (sale == null || sale.isEmpty()) {

            return false;
        }

        return true;
    }

    public Product getTotalPrice(Double adjustment, Product product) {

        double productValue;

        if (null != adjustment && adjustment.doubleValue() != 0.0) {

            product.setTotalPrice(adjustment);

        } else {

            productValue = product.calculatePrice(product.getProductQuantity(), product.getProductPrice());
            product.setTotalPrice(product.getTotalPrice() + productValue);
        }

        return product;
    }

    @Override
    public void logging(){

        if((processedMessages % 10) == 0 && processedMessages !=0) {

            Report.generateReportSales(salesByType);
        }

        if((processedMessages % 50) == 0 && processedMessages !=0) {

            Report.generateReportAdjustment(mAdjustedList);
        }
    }

}
