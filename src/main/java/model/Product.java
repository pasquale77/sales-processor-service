package model;

/**
 * Created by plongobucco on 24/02/18.
 *
 * This class describe a type of product
 *
 */
public class Product {

    private String productType;
    private int totalQuantity;
    private double totalPrice;
    private double productPrice;
    private int productQuantity;

    public Product(String productType) {
        this.totalPrice = 0.0;
        this.totalQuantity = 0;
        this.productType = productType;
    }

    public double calculatePrice(int productQuantity, double productPrice){

        return productQuantity * productPrice;
    }

    public void setTotalPrice(double totalPrice) {

        this.totalPrice = totalPrice;
    }

    public void setTotalQuantity(int quantity){

        this.totalQuantity += quantity;
    }

    public int getTotalQuantity() {

        return this.totalQuantity;
    }

    public double getTotalPrice() {

        return this.totalPrice;
    }

    public String getProductType() {

        return this.productType;
    }

    public void setProductType(String type) {

        this.productType = type;
    }

    public double getProductPrice() {

        return productPrice;
    }

    public void setProductPrice(double productPrice) {

        this.productPrice = productPrice;
    }

    public int getProductQuantity() {

        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {

        this.productQuantity = productQuantity;
    }

}
