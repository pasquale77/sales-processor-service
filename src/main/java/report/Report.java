package report;

import model.Product;

import java.util.HashMap;
import java.util.List;

/**
 * Created by plongobucco on 25/02/18.
 *
 * This utility class generates reports (sales report, adjustments report)
 *
 */
public class Report {

    /**
     * Creates the sales report
     *
     * @params salesByType
     *
     */
    public static void generateReportSales(HashMap<String, Product> salesByType){

        System.out.println("Log Sales - 10 sales appended to log: ");
        System.out.println("Product           Quantity   Price      ");
        salesByType.forEach((k,v) -> buildRow(v));
        System.out.println("----------------------------------");
        buildFooter(salesByType);
        System.out.println("\n");

    }

    /**
     * Creates the adjustments report
     *
     * @params adjustments
     *
     */
    public static void generateReportAdjustment(List<String> adjustments){

        System.out.println("Log Sales - 50 messages:");

        adjustments.forEach(System.out::println);

        System.exit(1);

    }

    /**
     * Creates a body for the report
     *
     * @params product
     *
     */
    private static void buildRow(Product product) {

        System.out.println(String.format("%-18s%-11d%-11.2f", product.getProductType(),
                product.getTotalQuantity(), product.getTotalPrice()));
    }

    /**
     * Creates a footer for the report
     *
     * @param salesByType
     *
     */
    private static void buildFooter(HashMap<String, Product> salesByType) {

        System.out.println(String.format("%-29s%-11.2f", "Total Sales",
                salesByType.values().stream().map(e -> e.getTotalPrice()).reduce(0.0, (x, y) -> x + y)));

        try {

            Thread.sleep(2000);

        } catch (InterruptedException e) {

            System.out.println("Error in tread.sleep: " + e.getMessage());
        }

    }

}
